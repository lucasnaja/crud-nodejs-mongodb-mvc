const express = require('express')
const server = express()
const cors = require('cors')
const mongoose = require('mongoose')
const requireAll = require('require-dir')

mongoose.connect('mongodb://localhost:27017/nodejs', { useNewUrlParser: true })
mongoose.set('useFindAndModify', false)
requireAll('./src/models')

server.use(express.json())
server.use(cors())
server.use('/api', require('./src/routes'))

server.listen(3000, () => console.log('Server initiallized.'))
