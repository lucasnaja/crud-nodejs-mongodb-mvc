const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    user_firstName: {
        type: String,
        required: true
    },
    user_lastName: {
        type: String,
        required: true
    },
    user_email: String,
    user_birth: {
        type: Date,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

mongoose.model('users', userSchema)
