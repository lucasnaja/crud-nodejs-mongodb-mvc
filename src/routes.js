const routes = require('express').Router()

const UserController = require('./controllers/Users')

// User routes
routes.get('/users', UserController.getAllUsers)
routes.get('/users/:user_id', UserController.getUser)
routes.post('/users', UserController.createUser)
routes.put('/users/:user_id', UserController.updateAllUserData)
routes.patch('/users/:user_id', UserController.updateSomeUserData)
routes.delete('/users/:user_id', UserController.deleteUser)

module.exports = routes
