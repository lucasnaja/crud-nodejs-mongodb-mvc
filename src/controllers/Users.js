const mongoose = require('mongoose')
const User = mongoose.model('users')

module.exports = {
    // POST method
    async createUser(req, res) {
        const user = await User.create(req.body)

        return res.status(201).json(user)
    },

    // GET method
    async getAllUsers(req, res) {
        const users = await User.find()

        return res.status(200).json(users)
    },

    // GET method
    async getUser(req, res) {
        const users = await User.findById(req.params.user_id)

        return res.status(200).json(users)
    },

    // PUT method
    async updateAllUserData(req, res) {
        const user = await User.findByIdAndUpdate(req.params.user_id, req.body, { new: true })

        return res.status(200).json(user)
    },

    // PATCH method
    async updateSomeUserData(req, res) {
        const user = await User.findByIdAndUpdate(req.params.user_id, req.body, { new: true })

        return res.status(200).json(user)
    },

    // DELETE method
    async deleteUser(req, res) {
        await User.findByIdAndDelete(req.params.user_id)

        const message = res.statusCode === 200 ? 'User deleted successfully.' : 'User deleted unsuccessfully.'
        return res.status(res.statusCode).json({ status: res.statusCode, message })
    }
}
