# CRUD - NodeJS and MongoDB

## Author: Lucas Bittencourt

### Used Dependencies
- express
- mongoose
- cors
- require-dir

### Used Dev Dependencies
- nodemon
- @types/express
- @types/mongoose
- @types/cors

### How to
- Test
    - `npm install`
    - Docker commands
        - `docker image pull mongo:latest`
        - `docker container run -d -p 27017:27017 --name mongodb mongo`
    - `npm start`
    - Insomnia or CURL


- CREATE
```sh
curl --request POST \
  --url http://localhost:3000/api/users \
  --header 'content-type: application/json' \
  --data '{
	"user_firstName": "Lucas",
	"user_lastName": "Bittencourt",
	"user_email": "lucasnaja0@hotmail.com",
	"user_birth": "1999/07/30"
}'
```
- GET ALL DATA
```sh
curl --request GET \
  --url http://localhost:3000/api/users
```

- GET SOME DATA
```sh
curl --request GET \
  --url http://localhost:3000/api/users/5d51754bd36feb219800a2c5
```

- UPDATE ALL DATA
```sh
curl --request PUT \
  --url http://localhost:3000/api/users/5d51754bd36feb219800a2c5 \
  --header 'content-type: application/json' \
  --data '{
	"user_firstName": "Lucas",
	"user_lastName": "Bittencourt",
	"user_email": "lucasnaja0@gmail.com",
	"user_birth": "1999/07/30"
}'
```

- UPDATE SOME DATA
```sh
curl --request PATCH \
  --url http://localhost:3000/api/users/5d51754bd36feb219800a2c5 \
  --header 'content-type: application/json' \
  --data '{
	"user_firstName": "Lucas"
}'
```

- DELETE
```sh
curl --request DELETE \
  --url http://localhost:3000/api/users/5d51754bd36feb219800a2c5
```